import React, {Component} from 'react';
import './App.css';
import Router from "./Router";
import {ProjectController} from "./data/Model";

class App extends Component {
  componentWillMount() {
    localStorage.setItem('projects', JSON.stringify(ProjectController.projects))
  }
  render() {
    return (
      <div className="App">
          <Router />
      </div>
    );
  }
}

export default App;
