// src/Authenticator.js
import React from 'react'
import { css } from 'glamor'
import { withRouter } from 'react-router-dom'
import styled from 'styled-components'
import SignIn from './SignIn'
import SignUp from './SignUp'
import {Color} from "./config/Config";

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    background-color: ${Color.primary}
    height: 100vh;
`;

const Container = styled.div`
    display: flex;
    color: white;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    // background-color: blue;
`;

const ModuleContainer = styled.div`
    // background-color: red;    
    padding-bottom: 100px;
`

class Authenticator extends React.Component {
    state = {
        showSignIn: true
    }
    switchState = (showSignIn) => {
        this.setState({
            showSignIn
        })
    }
    render() {
        const { showSignIn } = this.state
        return (
            <Wrapper>
                <Container>
                    <h1>Over Watch</h1>
                    <ModuleContainer>
                        {/*{*/}
                            {/*showSignIn ? (*/}
                                <SignIn />
                            {/*) : (*/}
                                {/*<SignUp />*/}
                            {/*)*/}
                        {/*}*/}
                    </ModuleContainer>
                    {/*<div {...css(styles.buttonContainer)}>*/}
                        {/*<p*/}
                            {/*{...css(styles.button, showSignIn && styles.underline)}*/}
                            {/*onClick={() => this.switchState(true)}*/}
                        {/*>Sign In</p>*/}
                        {/*<p*/}
                            {/*onClick={() => this.switchState(false)}*/}
                            {/*{...css(styles.button, !showSignIn && styles.underline)}*/}
                        {/*>Sign Up</p>*/}
                    {/*</div>*/}
                </Container>
            </Wrapper>
        )
    }
}

export default withRouter(Authenticator)

const styles = {
    buttonContainer: {
        display: 'flex',
        justifyContent: 'center'
    },
    button: {
        width: '100px',
        paddingBottom: '10px',
        cursor: 'pointer',
        borderBottom: '2px solid transparent'
    },
    underline: {
        borderBottomColor: '#ddd'
    }
}