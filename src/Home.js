// src/Home.js
import React from 'react'
import {withRouter} from 'react-router-dom'
import {Auth} from 'aws-amplify'
import {ListContainer} from "./components/ListContainer";
import {ProjectController, TemplateController} from "./data/Model";
import {LargeHeader} from "./components/LargeHeader";
import SideBar from "./components/SideBar";
import {BodyWrapper} from "./components/BodyWrapper";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            userFullName: '',
            jobs: []
        }
        this.nextPath = this.nextPath.bind(this)
    }

    componentWillMount() {
        ProjectController.poll()
    }
    componentDidMount() {
        Auth.currentUserInfo()
            .then(data => {
                this.setState({
                    username: data.username,
                    userFullName: TemplateController.getNameForUser(data.username),
                    jobs: TemplateController.getJobsForUser(data.username)
                })
            })
            .catch(err => console.log('error: ', err))

    }
    static compare(a,b) {
        if (a.title < b.title)
            return -1;
        if (a.title > b.title)
            return 1;
        return 0;
    }
    nextPath(path, title, job) {
        this.props.history.push({
            pathname: path,
            state: { title: title, job: job}
        });
    }

    render() {
        return (

            <div style={{
                width: '100vw',
                height: '100vh',
                display: 'flex',
                flexDirection: 'row',
                position: 'fixed',
            }}>
                <SideBar/>
                <div style={{

                    width: 'calc(100vw - 250px)'
                }}>
                <BodyWrapper/>
                </div>
                {/*<Header title='Projects Overview' userFullName={this.state.userFullName}/>*/}
                {/*<div style={{*/}
                    {/*paddingTop: '70px'}}>*/}
                    {/*{this.state.jobs.sort(this.compare).map(j => <JobItem nextPath={this.nextPath} job={j}/>)}*/}
                {/*</div>*/}
                {/*/!*<h1>Welcome {this.state.username}</h1>*!/*/}
            </div>

        )
    }
}

class Route1 extends React.Component {
    state = {
        username: '',
        userFullName: '',
        jobs: []
    }
    componentDidMount() {
        Auth.currentUserInfo()
            .then(data => {
                this.setState({
                    username: data.username,
                    userFullName: TemplateController.getNameForUser(data.username),
                    jobs: TemplateController.getJobsForUser(data.username)
                })
            })
            .catch(err => console.log('error: ', err))

    }
    render() {
        return (
            <div>
                <LargeHeader job={this.props.location.state.job} title={this.props.location.state.title} userFullName={this.state.userFullName}/>
                <div style={{paddingTop: '150px'}}>
                <ListContainer updateProject={() => {}} items={TemplateController.getItemsForJob(this.props.location.state.job)}/>
                <p onClick={() => {
                    Auth.signOut()
                        .then(() => {
                            this.props.history.push('/auth')
                        })
                        .catch(() => console.log('error signing out...'))
                }}>Sign Out</p>
                </div>
            </div>
        )
    }
}

Home = withRouter(Home)
Route1 = withRouter(Route1)

export {
    Home,
    Route1
}