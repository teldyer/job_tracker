import React from 'react'
import { css } from 'glamor'
import { Auth } from 'aws-amplify'
import styled from 'styled-components'

import { withRouter } from 'react-router-dom'
import {Color} from "./config/Config";

const errorColor = '#f90047'
const ErrorMessageContainer = styled.div`
    color: ${errorColor};
    font-style: italic;
    font-size: .9em;
    height: 12px;
`

class SignIn extends React.Component {
    state = {
        username: '',
        password: '',
        authCode: '',
        errorMessage: '',
    }
    onChange = (key, value) => {
        this.setState({
            [key]: value
        })
    }
    signIn = () => {
        Auth.signIn(this.state.username, this.state.password)
            .then(user => {
                this.props.history.push('/')
            })
            .catch(err => {
                console.log('error signing in...: ', err)
                this.setState({errorMessage: 'username or password incorrect'})
            })
    }
    handleKeypress = (e) => {
        if (e.key === 'Enter')
            this.signIn()
    }
    render() {
        return (
            <div {...css(styles.container)}>
                <ErrorMessageContainer>{this.state.errorMessage}</ErrorMessageContainer>
                <div {...css(styles.container)}>
                    <input
                        onChange={evt => this.onChange('username', evt.target.value)}
                        {...css(styles.input)}
                        placeholder='username'
                    />
                    <input
                        type='password'
                        onChange={evt => this.onChange('password', evt.target.value)}
                        {...css(styles.input)}
                        placeholder='password'
                        onKeyPress={this.handleKeypress}
                    />
                    <div {...css(styles.button)} onClick={this.signIn}>
                        <p {...css(styles.buttonText)}>Sign In</p>
                    </div>
                </div>
            </div>
        )
    }
}

const styles = {
    button: {
        color: 'black',
        padding: '10px 60px',
        backgroundColor: '#ddd',
        cursor: 'pointer',
        borderRadius: '3px',
        ':hover': {
            backgroundColor: '#ededed'
        }
    },
    buttonText: {
        margin: 0
    },
    input: {
        backgroundColor: Color.primary,
        color: 'white',
        height: 40,
        webkitAppearance: 'none',
        marginBottom: '10px',
        border: 'none',
        borderRadius: 0,
        outline: 'none',
        borderBottom: '2px solid ',
        borderBottomColor: Color.accent,
        fontSize: '16px',
        '::placeholder': {
            color: '#aaa'
        }
    },
    container: {
        flex: 1,
        paddingTop: '15px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    }
}

export default withRouter(SignIn)