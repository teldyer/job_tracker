import React from 'react'
import styled from 'styled-components'
import {Color} from "../config/Config";
import {ProjectController} from "../data/Model";
import moment from 'moment';
import {DatetimePickerTrigger} from 'rc-datetime-picker';

import "react-datepicker/dist/react-datepicker.css";
import {Link} from "react-router-dom";

const HelloContainer = styled.div`
    height: 100%;
    width: 100%;
    padding: 18px 32px 100px 50px;
    overflow: auto;
    box-sizing: border-box;
    @media (max-width: 700px) {
        padding: 18px;
        padding-bottom: 200px;
        overflow-y: scroll;   
        -webkit-overflow-scrolling: touch;
    }
`

export default class Hello extends React.Component {
    constructor(props) {
        super(props)
        this.state={
            moment: moment()
        }
    }
    handleChange = (moment) => {
        this.setState({
            moment
        });
    }
    render() {
        const shortcuts = {
            'Today': moment(),
            'Yesterday': moment().subtract(1, 'days'),
            'Clear': ''
        };
        return (
            <HelloContainer >
                <Label title='Blue Stakes'/>
                <DatetimePickerTrigger
                    shortcuts={shortcuts}
                    moment={this.state.moment}
                    onChange={this.handleChange}>
                    <input type="text" value={this.state.moment.format('YYYY-MM-DD HH:mm')} readOnly />
                </DatetimePickerTrigger>
                <ProjectCardContainer/>
            </HelloContainer>
        )

    }
}

const LabelContainerDiv = styled.div`
    width: 100%;
    padding-bottom: 18px;
    display: flex;
    align-items: center;
    color: ${Color.accent};
    text-transform: uppercase;
    font-size: 1.2em;
    font-weight: bold;
    
`

const Label = props => {
    return (
        <LabelContainerDiv>
            {props.title}
        </LabelContainerDiv>
    )
}

const ProjectCardContainerDiv = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    flex-wrap: wrap;
`

class ProjectCardContainer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            users: []
        }
    }
    componentWillMount() {
        this.setState({users: ProjectController.projects})
    }

    render() {
        return (
            <ProjectCardContainerDiv>
                {this.state.users.map(p => <ProjectCard project={p}/>)}
            </ProjectCardContainerDiv>
        )
    }
}

const ProjectCardDiv = styled.div`
    padding: 16px;
    width: 100%;
    height: 46px; 
    border-radius: 1px;
    background-color: white;
    margin-right: 18px;
    margin-bottom: 8px;
    text-align: left;
    // box-shadow: 0px 10px 14px #ddd;
    display: flex;
    border-radius: 4px;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    @media (max-width: 870px) {
        width: 100%;
        margin-right: 0;  
        height: auto;
        flex-wrap: wrap;
    }
`
const ProjectTitleDiv = styled.div`
    font-size: 1em;
    font-weight: bold;
    text-decoration: none;
`
const ProjectAssigneeDiv = styled.div`
`

const BottomDiv = styled.div`
    height: 60%;
    width: 200px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    font-weight: bold;
    @media (max-width: 870px) {
        width: 100%;
        margin-right: 0;  
        height: auto;
        justify-content: space-around;
        flex-wrap: wrap;
        margin-top: 20px;
    }
`
const green = '#00bf0a'
const ValidDate = styled.div`
    width: 50px;
    text-align: center;
    color: ${green};
    display: flex;
    flex-direction: row;
    justify-content: center;
`
const yellow = "#cccc33";
const UpdateDate = styled.div`
    width: 50px;
    text-align: center;
    color: ${yellow};
    display: flex;
    flex-direction: row;
    justify-content: center;
`
const red = "#f9001a"
const InvalidDate = styled.div`
    width: 50px;
    text-align: center;
    color: ${red};
    display: flex;
    flex-direction: row;
    justify-content: center;
`
const UpdateButtonDiv = styled.div`
    height:40px;
    width:100px;
    background-color: ${Color.button};
    box-shadow: 1px 1px 1px ${Color.buttonShadow};
    border-radius: 5px;
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
`
const getDaysDiff = (firstDate, secondDate) => {
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    return Math.round((firstDate - secondDate)/(oneDay));
}
const getDate = (date) => {
    var today = new Date();
    var newDate = new Date(date);
    var daysBetween = getDaysDiff(newDate, today);
    if (daysBetween >= 0 && daysBetween < 6) {
        return (
            <ValidDate>
                Valid
            </ValidDate>
        )
    } else if (daysBetween < 0) {
        return (
            <InvalidDate>
                Expired
            </InvalidDate>
        )
    } else if(daysBetween > 14) {
        return (
            <UpdateDate>
                Requested
            </UpdateDate>
        )
    }
    else {
        return (
            <ValidDate>
                {'Valid'}
            </ValidDate>
        )
    }
}
const ProjectCard = props => {
    return (
        <ProjectCardDiv>
            <div style={{}}>
                <Link to={'/project/' + props.project.id} style={{ textDecoration: 'none', color: 'inherit' }}>
                    <ProjectTitleDiv>
                        {props.project.title}
                    </ProjectTitleDiv>
                </Link>
                <ProjectAssigneeDiv>
                    <SmallAvatar date={props.project.blueStakeExpiration}/>
                </ProjectAssigneeDiv>
            </div>
            <BottomDiv>
                {getDate(props.project.blueStakeExpiration)}
                <UpdateButtonDiv>Update</UpdateButtonDiv>
            </BottomDiv>
        </ProjectCardDiv>
    )
}

const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
const BluestakesDatesWrapperdiv = styled.div`
    font-weight: bold;
    font-size: .9em;
    display: flex;
    align-items: center;
    color: #999;
`
const getDateRangeString = (date) => {
    var oneDay = 24*60*60*1000;
    var startDay = new Date(date - (14 * oneDay));
    var originalDay = new Date(date);
    return (monthNames[startDay.getMonth()]) + ' ' + startDay.getDate() + ' - ' + (monthNames[originalDay.getMonth()]) + ' ' + originalDay.getDate();

}
const SmallAvatar = props => {
    return (
        <BluestakesDatesWrapperdiv>{getDateRangeString(props.date)}</BluestakesDatesWrapperdiv>
    )
}

