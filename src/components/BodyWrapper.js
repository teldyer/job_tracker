import React from "react";
import styled from 'styled-components'
import {NavBar} from "./NavBar";
import {Color} from "../config/Config";
import {SideBar} from "./SideBar";
import {Route} from "react-router-dom";
import Hello from "./Hello";
import BlueStakesView from "./BlueStakesView";
import TeamMembersView from "./TeamMembersView";
import ProjectView from "./ProjectView";
import Home from "./Home";
import {ProjectController} from "../data/Model";
import MyProjectsView from "./MyProjectsView";

const WrapperDiv = styled.div`
    height: 100vh;
    width: 100%;
    @media (max-width: 700px) {
        width: 100vw;
      }
    background-color: ${Color.background};
`
ProjectController.init(() => {})
export const BodyWrapper = props => {
    return (
        <WrapperDiv>
            <Route
                path="/"
                exact
                render={(props) => <Home {...props}/>}
            />
            <Route
                path="/projects"
                exact
                render={(props) => <Hello {...props}/>}
            />
            <Route
                path="/my-projects"
                exact
                render={(props) => <MyProjectsView {...props}/>}
            />
            <Route
                path="/team-members"
                render={() => {
                    return (
                        <TeamMembersView/>
                    );
                }}
            />
            <Route
                path="/blue-stakes"
                render={() => {
                    return (
                        <BlueStakesView/>
                    );
                }}
            />
            <Route
                path="/project/:id"
                render={() => {
                    return (
                        <ProjectView/>
                    );
                }}
            />
            {props.children}
        </WrapperDiv>
    )
}