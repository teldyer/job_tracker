import React from "react";
import styled from "styled-components";
import {toast, ToastContainer} from 'react-toastify';

const Header = styled.div`
    width: 100%;
    
    // background-color: red;
    font-size: 1.2em;
    display: flex; 
    flex-direction: row;
    align-items: center;  

`;

const TitleDiv = styled.div`
    padding-left: 8px;
`;
const StyledSvg = styled.svg`
    padding-left: 24px;
    width: 30px;
    height: 30px;
    webkit-tap-highlight-color: rgba(0,0,0,0),
    &:hover{
        cursor: pointer;
    }
`

export class Checkable extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            completed: props.complete
        }
        this.toggleComplete = this.toggleComplete.bind(this)
    }
    toggleComplete(){
        if (!this.props.canEdit) {
            toast("This project isn't assigned to you", {type: toast.TYPE.ERROR, autoClose: 3500});
            return
        }
        this.setState({completed: !this.state.completed}, () => {
            this.props.completeItem(this.state.completed)
            if (this.state.completed)
                toast("You just completed " + this.props.title, {type: toast.TYPE.SUCCESS, autoClose: 2500});
            else
                toast("Scratch that", {type: toast.TYPE.SUCCESS, autoClose: 2500});
        })

    }
    render() {
        return (
            <Header>
                <ToastContainer/>
                <StyledSvg style={{
                        paddingLeft:'24px',
                        width:'30px',height:'30px',
                        WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)',
                        transition: 'all 50 ease-out'
                    }}
                           cursor='pointer'
                     viewBox="0 0 24 24"
                     onClick={this.toggleComplete}
                >
                    <a cursor="pointer">
                    <path style={{
                        transition: 'all 100ms ease-in-out'
                    }}
                          fill={this.state.completed ? "#07d635" : "#d7d7d7"} d="M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2M12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12A8,8 0 0,0 12,4M12,19C10.1,19 8.3,18.2 7.1,16.9C5.9,15.6 5,13.9 5,12C5,10.1 5.8,8.3 7.1,7.1C8.4,5.9 10.1,5 12,5C13.9,5 15.7,5.8 16.9,7.1C18.1,8.4 19,10.1 19,12C19,13.9 18.2,15.7 16.9,16.9C15.6,18.1 13.9,19 12,19Z" />
                    </a>
                </StyledSvg>
                <TitleDiv>{this.props.title}</TitleDiv>
            </Header >
        )
    }
}