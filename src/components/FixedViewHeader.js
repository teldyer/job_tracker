import React from 'react'
import styled from "styled-components";
import {Color} from "../config/Config";

const LabelContainerDiv = styled.div`
    background-color: white; 
    z-index: 10;
    position:fixed;
    padding: 18px 30px 18px 30px;
    width: calc(100vw - 250px);
    height: 90px;
    display: flex;
    align-items: center;
    box-sizing: border-box;
    justify-content: space-between;
    color: ${Color.accent};
    text-align: left;
    font-size: 1.2em;
    font-weight: bold;
    @media (max-width: 700px) {
        padding: 18px;
        width: 100vw;
        background-color: ${Color.primary}
        border-top: solid 1px ${Color.primary};
    }
`

export const FixedViewHeader = (props) => {
    return (
        <LabelContainerDiv>
            {props.children}
        </LabelContainerDiv>
    )
}