import React from "react";
import styled from 'styled-components';

const color = '#2e3234'
const Container = styled.div`
    padding: 16px;
    position: fixed;
    width: 100%;
    height: 70px;
    background-color: #2c3e51;
    color: white;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    font-size: 1.5em;
    box-shadow: 0px 1px 4px #888;  
    font-weight: 500;
    box-sizing: border-box;  
`;

const Column = styled.div`
    display: flex;
    justify-content: center;
    // align-items: center;
`;

const CenterColumn = styled.div`
    display: flex;
    justify-content: center;
    // align-items: center;
`;
const orange = '#9c9c9c'
const AvatarContainer = styled.div`
    width: 40px;
    height: 40px;
    border-radius: 20px;
    background-color: ${orange};
    display: flex;
    align-items: center;
    justify-content: center;
`

const getInitials = (name) => {
    if (name === undefined)
        return ''
    var initials = name.match(/\b\w/g) || [];
    initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
    return initials;
}


export const Header = (props) => {
    return (
        <Container>
            {/*<Column>*/}
                {/*<svg style={{width:'30px', height:'30px', marginTop: '-4px'}} viewBox="0 0 24 24">*/}
                    {/*<path fill="#fff" d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z" />*/}
                {/*</svg>*/}
            {/*</Column>*/}
            <CenterColumn>
                {props.title}
            </CenterColumn>
            <Column>
                <AvatarContainer>
                {getInitials(props.userFullName)}
                </AvatarContainer>

            </Column>
        </Container>
    )
};