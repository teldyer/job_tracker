import React from 'react'
import styled from 'styled-components'
import {Color} from "../config/Config";
import {ProjectController, TemplateController} from "../data/Model";
import {StatusBar} from "./StatusBar";
import {Link, withRouter} from "react-router-dom";
import {Auth} from 'aws-amplify'
import {ViewHeader} from "./ViewHeader";
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const HelloContainer = styled.div`
    height: calc(100% - 70px);
    width: 100%;
    padding: 0px;
    box-sizing: border-box;
    min-height: 686px;
    @media (max-width: 700px) {
        padding: 18px;
        // padding-bottom: 200px; 
        overflow-y: scroll; 
        -webkit-overflow-scrolling: touch; 
    }
    @media (max-height: 686px) {
        height: 100%;
        overflow-y: scroll; 
        -webkit-overflow-scrolling: touch; 
    }
`

const WidgetWrapper = styled.div`
    display: flex;
    padding: 30px;
    box-sizing: border-box;
    flex-direction: row;
    justify-content: space-between;
    height: 100%;
    width: 100%;
    margin-top: -10px;
`

const UserStatsContainer = styled.div`
    width: 30%;
    height: 100%;
`
const TeamStatsContainer = styled.div`
    width: calc(70% - 30px);
    height: 100%;
`
const StyledSvg = styled.svg`
    padding-left: 12px;
    width: 20px;
    height: 20px;
    webkit-tap-highlight-color: rgba(0,0,0,0),
   `

const StyledNotificationSvg = styled.svg`
    width: 14px;
    height: 14px;
    webkit-tap-highlight-color: rgba(0,0,0,0),
   `

const StyledNewSvg = styled.svg`
margin-left: 4px;
    width: 24px;
    height: 24px;
    webkit-tap-highlight-color: rgba(0,0,0,0),
   `
const SVGContainer = styled.div`
    cursor: pointer;
`
const StyledInput = styled.input`
    color: #999;
    font-family: 'Lato', sans-serif, 'Archivo Narrow',"Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;
    font-size: .9em;
    font-weight: bold;
    border: none;
    margin-left: 8px;
    width: 90px;
    &:focus{
        outline: none;
    }
`
const PointerDiv = styled.div`
    &:hover {
        cursor: pointer;
    }
`
class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            userFullName: '',
            projects: [],
            recentlyCompleted: [],
            editPhoneNumberDisabled: true,
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleBlur = this.handleBlur.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleOnKeyUp = this.handleOnKeyUp.bind(this)
        this.poll = this.poll.bind(this)
    }
    componentWillMount() {
        var projects = JSON.parse(localStorage.getItem('projects'))
        Auth.currentUserInfo()
            .then(data => {
                this.setState({
                    username: data.username,
                    userFullName: TemplateController.getNameForUser(data.username),
                    phoneNumber: ProjectController.notificationPhoneNumber,
                    editingPhoneNumber: false,
                    editPhoneNumberDisabled: true,
                    users: projects,
                    recentlyCompleted: getRecentlyCompleted(projects, (Math.round(window.innerHeight / 80)), new Date().setHours(0,0,0,0))
                })
            })
            .catch(err => console.log('error: ', err))
        this.poll()
    }
    poll() {
        setTimeout(() => {
            var projects = JSON.parse(localStorage.getItem('projects'))
            this.setState({
                users: projects,
                recentlyCompleted: getRecentlyCompleted(projects, (window.innerHeight < 760 ? 5 : 6), new Date().setHours(0,0,0,0))
            }, this.poll()) // would hit the API here
        }, 2000)
    }
    activateEditNumber() {
        this.setState({editPhoneNumberDisabled: false, editingPhoneNumber: true}, () => this.nameInput.focus())
    }
    deactivateEditNumber() {
        if (this.state.editingPhoneNumber)
            this.handleSubmit()

    }
    handleChange(event) {
        if (event.keyCode === 13)
            alert('enter')
        var value = event.target.value
        value = value.replace(/[^0-9 ]/g, "");
        this.setState({phoneNumber: '+1' + value});
    }
    handleOnKeyUp(e) {
        if (e.keyCode === 13)
            this.handleSubmit()
    }
    formatNumber(number) {
        if (number)
            return number.slice(2, number.length)
        return number
    }
    handleBlur(event) {
        if (!this.state.editingPhoneNumber)
            return
        this.setState({editingPhoneNumber: false}, () => {
            this.handleSubmit()
        })
    }
    handleSubmit(event) {
        this.setState({editingPhoneNumber: false, editPhoneNumberDisabled: true}, () => {
            // this.nameInput.blur()
            if (this.state.phoneNumber.length === 12) {
                toast("Phone number saved!", { type: toast.TYPE.SUCCESS, autoClose: 2000 });
                ProjectController.notificationPhoneNumber = this.state.phoneNumber
            }
            else {
                toast("Invalid phone number", { type: toast.TYPE.ERROR, autoClose: 2000 });
            }
            this.setState({
                phoneNumber: ProjectController.notificationPhoneNumber,
            })
        })

    }
    render() {
        return (
                <HelloContainer>
                    <ToastContainer autoClose={2500}/>
                    <ViewHeader>
                        <div style={{width: '30%', textTransform: 'uppercase'}}>
                            Dashboard
                        </div>
                        <div style={{width: '50%', color: '#999', fontSize: '.9em', textAlign: 'right', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',}}>
                            <StyledNotificationSvg style={{marginRight: '4px'}}
                                viewBox="0 0 24 24">
                                <path fill='#E5C700'  d="M21,19V20H3V19L5,17V11C5,7.9 7.03,5.17 10,4.29C10,4.19 10,4.1 10,4A2,2 0 0,1 12,2A2,2 0 0,1 14,4C14,4.1 14,4.19 14,4.29C16.97,5.17 19,7.9 19,11V17L21,19M14,21A2,2 0 0,1 12,23A2,2 0 0,1 10,21M19.75,3.19L18.33,4.61C20.04,6.3 21,8.6 21,11H23C23,8.07 21.84,5.25 19.75,3.19M1,11H3C3,8.6 3.96,6.3 5.67,4.61L4.25,3.19C2.16,5.25 1,8.07 1,11Z" />
                            </StyledNotificationSvg>
                            SMS Notification Number:
                            <StyledInput type="text"
                                                                  ref={(input) => { this.nameInput = input; }}
                                                                  onChange={this.handleChange}
                                                                  disabled={this.state.editPhoneNumberDisabled}
                                                                  onBlur={this.handleBlur}
                                                                  onSubmit={this.handleSubmit}
                                                                  onKeyDown={this.handleOnKeyUp}
                                                                  value={this.formatNumber(this.state.phoneNumber)}/>

                                          {!this.state.editingPhoneNumber &&
                                          <SVGContainer onClick={() => this.activateEditNumber()}>
                                              <StyledSvg
                                                  viewBox="0 0 24 24">
                                          <path fill='#999' d="M14.06,9L15,9.94L5.92,19H5V18.08L14.06,9M17.66,3C17.41,3 17.15,3.1 16.96,3.29L15.13,5.12L18.88,8.87L20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18.17,3.09 17.92,3 17.66,3M14.06,6.19L3,17.25V21H6.75L17.81,9.94L14.06,6.19Z" />
                                              </StyledSvg>
                                          </SVGContainer>}
                                          {this.state.editingPhoneNumber &&
                                              <SVGContainer onClick={() => this.deactivateEditNumber()}>
                                              <StyledSvg
                                              viewBox="0 0 24 24">
                                          <path fill='#999' d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
                                              </StyledSvg>
                                              </SVGContainer>}
                        </div>
                        {/*<PointerDiv style={{width: 'auto' , cursor: 'pointer', color: Color.accent, fontSize: '.9em', textAlign: 'right', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',}}>*/}
                            {/*New Project*/}
                            {/*<StyledNewSvg*/}
                                                   {/*viewBox="0 0 24 24">*/}
                                {/*<path fill={Color.accent}  d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" />*/}
                            {/*</StyledNewSvg>*/}
                        {/*</PointerDiv>*/}
                    </ViewHeader>

                        {/*<Header/>*/}
                        <WidgetWrapper>
                            <UserStatsContainer>
                                <UserStats state={this.state}/>
                            </UserStatsContainer>
                            <TeamStatsContainer>
                                <TeamStats recentlyCompleted={this.state.recentlyCompleted} projects={this.state.users}/>
                            </TeamStatsContainer>
                        </WidgetWrapper>

                </HelloContainer>
        )

    }
}

export default withRouter(Home);

const UserStatsWrapper = styled.div`
    width: 100%;
    height: 100%;
    border-radius: 4px;
`

const getUserProjects = (projects, assignee) => {
    if (!projects)
        return []
    console.log(projects)
    console.log(assignee)
    return projects.filter(p => TemplateController.getUserById(p.assignee).name === assignee)
}
const getCompletedItems = (projects) => {
    var time = new Date().setHours(0,0,0,0)
    var completed_count = 0;
    for (var i = 0; i < projects.length; ++i) {
        var p = projects[i]
        if (p.items) {
            for (var j = 0; j < p.items.length; ++j) {
                var item = p.items[j]
                if (item.date_complete) {
                    if (item.date_complete > time) {
                        ++completed_count
                    }
                }
            }
        }
    }
    return completed_count
}

const UserStats = props => {
    var userProjects = getUserProjects(props.state.users, props.state.userFullName)
    var itemsCompleted = getCompletedItems(userProjects)
    return (
        <UserStatsWrapper>
            <div style={{
                width: '100%',
                marginBottom: '10px',
                borderRadius: '4px 4px 0px 0px',
                display: 'flex',
                alignItems: 'center',
                boxSizing: 'border-box',
                color: '#999',
                fontWeight: 'bold'

            }}>
                My Progress
            </div>
            <div style={{
                height: '90%',
                backgroundColor: 'white',
                borderRadius: '4px',
                display: 'flex',
                flexDirection: 'column',
                textAlign: 'left',
                padding: '18px',
                boxSizing: 'border-box',
            }}>

                {/*card stuff here*/}
                <div style={{fontWeight: 'bold', height: '28px',}}>Welcome {props.state.userFullName}</div>
                <div style={{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between'}}>
                    <div style={{minWidth: '100px',width: '45%', display: 'flex', justifyContent: 'space-between', flexDirection: 'column', alignItems: 'center', padding: '4px', backgroundColor: '#f9f9f9'}}>
                        <div style={{textAlign: 'center', fontSize:'.8em', color: '#999', height: '20%'}}/>
                        <div style={{fontSize: '5em', fontWeight: 'bold', color: '#00bf0a'}}>{itemsCompleted}</div>
                        <div style={{textAlign: 'center', fontSize:'.8em', color: '#999', height: '20%'}}>Items completed today</div>
                    </div>
                    <div style={{minWidth: '100px',width: '45%', display: 'flex', justifyContent: 'space-between', flexDirection: 'column', alignItems: 'center', padding: '4px', backgroundColor: '#f9f9f9'}}>
                        <div style={{textAlign: 'center', fontSize:'.8em', color: '#999', height: '20%'}}/>
                        <div style={{fontSize: '5em', fontWeight: 'bold', color: Color.accent}}>{userProjects.length}</div>
                        <div style={{textAlign: 'center', fontSize:'.8em', color: '#999', height: '20%'}}>Projects assigned to you</div>
                    </div>
                </div>
                <div style={{marginTop: '40px', color: '#999'}}>{itemsCompleted > 0 ? 'Get right back to it' : 'Get started right away'}</div>
                {userProjects.sort(compareProjectName).map(p =>

                    <div style={{height: '40px', fontWeight: 'bold', display: 'flex', flexDirection: 'row', alignItems:'center', justifyContent: 'space-between'}}>
                        <Link to={'/project/' + p.id} style={{ textDecoration: 'none', width: '100%', fontWeight: 'bold', display: 'flex', flexDirection: 'row', alignItems:'center', justifyContent: 'space-between', color: 'inherit' }}>
                        {p.title}
                        <svg style={{width: '24px', height: '24px', marginTop: '-3px', marginLeft: '10px'}} viewBox="0 0 24 24">
                            <path fill="#999" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" />
                        </svg>

                    </Link>
                    </div>
                )}
                <Link to="/my-projects" style={{ textDecoration: 'none', color: '#999', fontWeight: 'bold', display: 'flex', flexDirection: 'row', alignItems:'center', justifyContent: 'flex-end', marginTop: 'auto'}}>My projects
                    <svg style={{width: '24px', height: '24px', marginTop: '2px',marginLeft: '10px'}} viewBox="0 0 24 24">
                        <path fill="#999" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" />
                    </svg>
                </Link>
            </div>
            <div style={{
                width: '100%',
                borderRadius: '4px 4px 0px 0px',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-end',
                boxSizing: 'border-box',
                color: '#999',
                fontWeight: 'bold',
                fontSize: '1em',
                marginTop: '10px'

            }}>
            </div>
        </UserStatsWrapper>
    )
}

const compareProjectName = (a,b) => {
    if (a.title < b.title)
        return -1;
    if (a.title > b.title)
        return 1;
    return 0;
}

const compare = (a,b) => {
    if (a.date_completed > b.date_completed)
        return -1;
    if (a.date_completed < b.date_completed)
        return 1;
    return 0;
}

const getRecentlyCompleted = (projects, numItems, startDate) => {
    var windowH = window.innerHeight - 150
    numItems = (Math.round(windowH / 80))
    // startDate = (startDate.setHours(0,0,0,0))
    if (projects === undefined)
        return []
    var recents = []
    for (var i = 0; i < projects.length; ++i) {
        var completed_count = 0;
        var most_recent_date = startDate;
        var most_recent_name = 'no items yet';
        var p = projects[i]
        if (p.items) {
            for (var j = 0; j < p.items.length; ++j) {
                var item = p.items[j]
                if (item.date_complete) {
                    if (item.date_complete > startDate) {
                        completed_count++
                        if (item.date_complete > most_recent_date) {
                            most_recent_date = item.date_complete
                            most_recent_name = '"' + item.title + '"';
                        }
                    }
                }
            }
        }
        recents.push({
            project_title: p.title,
            assignee: TemplateController.getUserById(p.assignee).name,
            date_completed: most_recent_date,
            item_title: most_recent_name,
            completed_count: completed_count - 1,
            percentage: ProjectController.getProjectPercentage(p.items),
            project: p
        })
    }
    var result = recents.sort(compare);

    return result.slice(0, numItems);
}

const TeamStats = props => {
    return (
        <div style={{
            width: '100%',
            height: '100%',
            borderRadius: '4px',
        }}>
            <div style={{
                width: '100%',
                marginBottom: '10px',
                borderRadius: '4px 4px 0px 0px',
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                boxSizing: 'border-box',
                color: '#999',
                fontWeight: 'bold'

            }}>
                Team Progress Today

                        <Link to="/projects" style={{ textDecoration: 'none', color: 'inherit', display: 'flex', flexDirection: 'row', alignItems:'center'}}>All team projects
                        <svg style={{width: '24px', height: '24px', marginTop: '2px', marginLeft: '10px'}} viewBox="0 0 24 24">
                            <path fill="#999" d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" />
                        </svg>
                    </Link>
            </div>
            <div style={{
                height: '90%',
                borderRadius: '4px',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'flex-start'
            }}>
                {props.recentlyCompleted.map(i =>
                    <ProjectProgressWidget data={i}/>
                )}
            </div>
            <div style={{
                width: '100%',
                borderRadius: '4px 4px 0px 0px',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-end',
                boxSizing: 'border-box',
                color: '#999',
                fontWeight: 'bold',
                fontSize: '1em',
                marginTop: '10px'

            }}>
            </div>
        </div>
    )
}

const ProjectProgressWidget = props => {
    return (
        <Link to={'/project/' + props.data.project.id} style={{ textDecoration: 'none', color: 'inherit' }}>
        <div style={{display: 'flex', flexDirection: 'row',
            backgroundColor: 'white',
            borderRadius: '4px',
            marginBottom: '8px',
        width: '100%'}}>
            <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', width: '80px'}}>
                <svg style={{width:'25px',height:'25px'}} viewBox="0 0 24 24">
                    <path fill="#999" d="M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2M12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12A8,8 0 0,0 12,4M11,16.5L6.5,12L7.91,10.59L11,13.67L16.59,8.09L18,9.5L11,16.5Z" />
                </svg>
            </div>
            <div style={{
                padding: '18px',
                paddingLeft: '0px',
                width: '100%',
                minHeight: '70px',
                height: '15%',
                maxHeight: '80px',
                boxSizing: 'border-box',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                justifyContent: 'center',
            }}>
                <div style={{fontSize: '1em', fontWeight: 'medium', color: 'black',marginBottom: '8px'}}>
                    {props.data.assignee + ' completed '} <b>{props.data.item_title}</b>
                    {(props.data.completed_count > 0 ? (' plus ' + props.data.completed_count + ' other items') : '')}
                </div>
                <div style={{width: '100%'}}>
                    <div style={{width: '100%', fontSize: '1em', fontWeight: 'bold', color: '#999', marginBottom: '4px', display:'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                        <div style={{fontSize: '.9em'}}>{props.data.project_title}</div>
                        <div style={{fontSize: '.9em'}}>{props.data.percentage + '%'}</div>
                    </div>
                </div>

                <div style={{width: '100%'}}>
                    <StatusBar percentage={props.data.percentage} height={3}/>
                </div>
            </div>
        </div>
        </Link>
    )
}