import React, {Component} from "react";
import styled from 'styled-components';
import {Checkable} from "./Checkable";
import {ProjectController, TemplateController} from "../data/Model";
import {Auth} from 'aws-amplify'
import {checkProjectComplete} from "../config/Config";

import {toast, ToastContainer} from 'react-toastify';

const Wrapper = styled.div`
    padding-bottom: 1px;
    width: 100%;   
    display: flex;
    justify-content: center;
`;

const Container = styled.div`
    background-color: #fff;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    width: 95vw;
    height: ${props => props.expanded ? props.subItemLength * 90 + 'px' : '70px'}
    min-height: 60px;
    transition: all 240ms ease-out;
    color: black;
    border-radius: 4px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    box-shadow: 0px 10px 14px #ddd;   
    font-size: .9em;
    overflow: hidden;
`;

const Header = styled.div`
    padding-top: 50px;
    padding-left: 28px;
    height: 60px;
    width: 100%;
    margin-bottom: 50px;
    
    font-size: 1em;
    // background-color: red;
    display: flex; 
    flex-direction: row;
    align-items: center;
    

`;


const useTwilio = true

export class JobCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: false,
            title: props.item.title ? props.item.title : props.item,
            subItems: props.item.items ? props.item.items : [],
            completed: props.item.date_complete,

        };
        this.expand = this.expand.bind(this)
        this.completeItem = this.completeItem.bind(this)
    }
    componentWillMount() {
        var project = ProjectController.getProjectById(this.props.projectId)
        var canEdit = TemplateController.getUserById(project.assignee).name === this.state.userFullName

        Auth.currentUserInfo()
            .then(data => {
                var userFullName = TemplateController.getNameForUser(data.username)
                this.setState({
                    username: data.username,
                    userFullName: userFullName,
                    users: ProjectController.projects,
                    canEdit: TemplateController.getUserById(project.assignee).name === userFullName
                })
            })
            .catch(err => console.log('error: ', err))
    }
    expand(){
        this.setState({expanded: !this.state.expanded})
    }



    completeItem(completedStatus){

        this.setState({completed: !this.state.completed})
        ProjectController.completeItem(this.props.projectId, this.state.title, completedStatus)
        this.props.updateProject()
        var project = ProjectController.getProjectById(this.props.projectId);
        var projectTitle = project.title.length > 21?  project.title.slice(0,21) + '...' : project.title
        var message = ''
        var name = project.assignee_name.length > 8 ? project.assignee_name.slice(0, 8) + '...' : project.assignee_name
        if (!useTwilio)
            return
        if (completedStatus) {
            message = `\u2002    ${projectTitle}\n✅\u0009${this.state.title}\n\u0009completed by ${name}`

        }
        else
            message = `\u2002    ${projectTitle}\n❌\u0009${this.state.title}\n\u0009unchecked by ${name}`
        fetch('http://18.237.167.72:3000/sms', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                to: ProjectController.notificationPhoneNumber,
                message: message
            }),
        })
        .then(function(response){
            checkProjectComplete(project);
            }).then(function(body){

            });
        }
    render() {
        return (
            <Wrapper>
                <Container expanded={this.state.expanded} subItemLength={this.state.subItems.length}>
                    {
                        this.state.subItems.length > 0 ?
                        <Header onClick={this.expand}>{this.state.title}</Header>:
                        <Checkable canEdit={this.state.canEdit} complete={this.state.completed} title={this.state.title} completeItem={this.completeItem}/>
                    }
                    {this.state.subItems.map(s => <Checkable canEdit={this.state.canEdit} complete={s.date_complete} title={s.title} completeItem={this.completeItem}/>)}
                </Container>
            </Wrapper>
        )
    }
}

