import React from "react";
import styled from 'styled-components';
import {JobCard} from "./JobCard";
import {TemplateController} from "../data/Model";

const Container = styled.div`
    color: white;
    display: flex;
    width: 100%;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
`;

export class JobCardContainer extends React.Component {

    render() {
        return (
            <Container>
                {this.props.children}
            </Container>
        )
    }

};
