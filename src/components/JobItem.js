import React, {Component} from "react";
import styled from 'styled-components';
import {StatusBar} from "./StatusBar";
import {TemplateController} from "../data/Model";

const Wrapper = styled.div`
    padding-bottom: 0px;
    width: 100%;   
    margin: auto;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const Container = styled.div`
    background-color: #fff;
    padding: 20px;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    width: 100%;
    height: 80px;
    min-height: 70px;
    transition: all 240ms ease-out;
    color: black;
    border-radius: 0px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
    border-bottom: solid 1px #eee;
    box-shadow: 0px 10px 20px #bbb;  
    overflow: hidden;
`;

const Header = styled.div`
    width: 80%;
    
    // background-color: red;
    font-size: 1em;
    font-weight: 200;
    text-align: left;
    display: flex; 
    flex-direction: column;
    justify-content: space-between;
    align-items: flex-start;
    width: 100%;
    height: 50px;
`;

export class JobItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: false,
            job: props.job,
            jobPercentage: TemplateController.getJobPercentage(props.job)
        };
        this.expand = this.expand.bind(this)
        this.complete = this.complete.bind(this)
    }
    expand(){
        this.props.nextPath('/route1', this.state.job.title, this.state.job)
    }
    complete(){
        this.setState({completed: !this.state.completed})
    }
    render() {
        return (
            <Wrapper>
                <Container onClick={this.expand} expanded={this.state.expanded}>
                        <Header>
                            {this.state.job.title}
                            <StatusBar percentage={this.state.jobPercentage}/>
                        </Header>
                </Container>
            </Wrapper>
        )
    }
}

