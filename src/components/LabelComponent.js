import styled from "styled-components";
import {Color} from "../config/Config";
import React from "react";

const LabelContainerDiv = styled.div`
    width: 100%;
    padding-bottom: 18px;
    display: flex;
    align-items: center;
    color: ${Color.accent};
    text-transform: uppercase;
    font-size: 1.2em;
    font-weight: bold;
    
`

export const LabelComponent = (props) => {
    return (
        <LabelContainerDiv>
            {props.title}
        </LabelContainerDiv>
    )
}