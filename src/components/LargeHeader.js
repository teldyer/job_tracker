import React from "react";
import styled from 'styled-components';
import {TemplateController} from "../data/Model";
import {SquareStatusBar} from "./SquareStatusBar";

const color = '#2e3234'
const Container = styled.div`
    width: 100vw;
    height: 120px;
    background-color: #2c3e51;
    color: white;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    font-size: 1.5em;
    // box-shadow: 0px 1px 4px #888;  
    font-weight: 500;
`;

const Column = styled.div`
    padding: 16px;
    display: flex;
    justify-content: center;
    // align-items: center;
`;

const CenterColumn = styled.div`
    display: flex;
    justify-content: center;
    // align-items: center;
`;
const orange = '#9c9c9c'
const AvatarContainer = styled.div`
    width: 40px;
    height: 40px;
    border-radius: 20px;
    background-color: ${orange};
    display: flex;
    align-items: center;
    justify-content: center;
`

const RowDiv = styled.div`
    width: 100%;
    justify-content: space-between;
    display: flex;
    flex-direction: row;
`

const TitleDiv = styled.div`
    margin-bottom: 16px;
    margin-left: 16px;
    
`

const getInitials = (name) => {
    if (name === undefined)
        return ''
    var initials = name.match(/\b\w/g) || [];
    initials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
    return initials;
}


export const LargeHeader = (props) => {
    return (
        <div style={{position: 'fixed'}}>
        <Container>
            <RowDiv>
                <Column>
                    <svg style={{width:'30px', height:'30px', marginTop: '-4px'}} viewBox="0 0 24 24">
                        <path fill="#fff" d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z" />
                    </svg>
                </Column>
                <CenterColumn>
                </CenterColumn>
                <Column>
                    <AvatarContainer>
                        {getInitials(props.userFullName)}
                    </AvatarContainer>

                </Column>
            </RowDiv>
            <RowDiv>
                <TitleDiv>
                    {props.title}
                </TitleDiv>
            </RowDiv>
        </Container>
        <SquareStatusBar percentage={TemplateController.getJobPercentage(props.job)}/>
        </div>
    )
};