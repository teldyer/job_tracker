import * as React from "react";
import {JobCardContainer} from "./JobCardContainer";
import {JobCard} from "./JobCard";


export class ListContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: props.items,
            project: props.project
        }
    }

    render() {
        return (
            <JobCardContainer>
                {this.state.list.map(item => {
                    if (item.complete !== undefined)
                        return <JobCard updateProject={this.props.updateProject} projectId={this.state.project.id} item={item}/>
                    else
                        return <JobCard updateProject={this.props.updateProject} projectId={this.state.project.id} item={item}/>
                })}
            </JobCardContainer>
        )
    }
}