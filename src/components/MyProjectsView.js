import React from 'react'
import styled from 'styled-components'
import {Color} from "../config/Config";
import {ProjectController, TemplateController} from "../data/Model";
import {StatusBar} from "./StatusBar";
import {withRouter} from "react-router-dom";
import {Auth} from 'aws-amplify'
import {ViewHeader} from "./ViewHeader";
import {PointerDiv, ViewBodyWrapper, ViewWrapper} from "../config/Components";
import {FixedViewHeader} from "./FixedViewHeader";

const StyledNewSvg = styled.svg`
margin-left: 4px;
    width: 24px;
    height: 24px;
    webkit-tap-highlight-color: rgba(0,0,0,0),
   `

class MyProjectsView extends React.Component {
    render() {
        return (
            <ViewWrapper>
                <FixedViewHeader>
                    <div style={{width: '30%', textTransform: 'uppercase'}}>
                        My Projects
                    </div>
                    {/*<PointerDiv style={{width: 'auto' , cursor: 'pointer', color: Color.accent, fontSize: '.9em', textAlign: 'right', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',}}>*/}
                        {/*New Project*/}
                        {/*<StyledNewSvg*/}
                            {/*viewBox="0 0 24 24">*/}
                            {/*<path fill={Color.accent}  d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" />*/}
                        {/*</StyledNewSvg>*/}
                    {/*</PointerDiv>*/}
                </FixedViewHeader>
                <ViewBodyWrapper>
                    <ProjectCardContainerWithRouter/>
                </ViewBodyWrapper>
            </ViewWrapper>
        )

    }
}

export default withRouter(MyProjectsView);
const LabelContainerDiv = styled.div`
    width: 100%;
    padding-bottom: 18px;
    display: flex;
    align-items: center;
    color: ${Color.accent};
    text-transform: uppercase;
    font-size: 1.2em;
    font-weight: bold;
    
`

const Label = props => {
    return (
        <LabelContainerDiv>
            {props.title}
        </LabelContainerDiv>
    )
}

const ProjectCardContainerDiv = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    padding-left: 18px;
    box-sizing: border-box;
     @media (max-width: 1047px) {
        padding-left: 0px;  
    }
`

const compare = (a,b) => {
    if (a.title < b.title)
        return -1;
    if (a.title > b.title)
        return 1;
    return 0;
}

class ProjectCardContainer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            projects: []
        }
        this.navigateToProjectView = this.navigateToProjectView.bind(this)
    }
    componentWillMount() {
        var projects = JSON.parse(localStorage.getItem('projects'))
        Auth.currentUserInfo()
            .then(data => {
                var userFullName = TemplateController.getNameForUser(data.username)
                this.setState({
                    userFullName: userFullName,
                    projects: projects.filter(p => TemplateController.getUserById(p.assignee).name === userFullName),
                })
            })
            .catch(err => console.log('error: ', err))
        this.poll()
    }
    poll() {
        setTimeout(() => {
            var projects = JSON.parse(localStorage.getItem('projects'))
            this.setState({
                projects: projects,
            }, this.poll()) // would hit the API here
        }, 2000)
    }
    navigateToProjectView(project) {
        var url = 'http://18.237.167.72:3000/project?id=' + project.id
        fetch(url, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                },
            },
        ).then(response => {
            if (response.ok) {
                response.json().then(json => {
                    var proj = json.filter(p => p.id = this.props.match.params.id)[0]

                    this.props.history.push({
                        pathname: '/project/' + project.id,
                        state: {project: proj}
                    });
                });
            }
        });
        ProjectController.init(() => {})
        localStorage.setItem('selectedProject', JSON.stringify(project));
    }
    render() {
        return (
            <ProjectCardContainerDiv>
                {this.state.projects.filter(p => TemplateController.getUserById(p.assignee).name === this.state.userFullName).sort(compare).map((p) => {
                    return (<ProjectCard navigate={this.navigateToProjectView} project={p} />)
                })}

            </ProjectCardContainerDiv>
        )
    }
}
const ProjectCardContainerWithRouter = withRouter(ProjectCardContainer);


const ProjectCardDiv = styled.div`
    padding: 16px;
    width: 310px;
    height: 140px; 
    border-radius: 1px;
    background-color: white;
    margin-right: 18px;
    margin-bottom: 18px;
    text-align: left;
    // box-shadow: 0px 20px 14px #ddd;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    transition: all .1s ease-in-out;
    position: relative;
    &:hover{
        transform: scale(.98, .98);
        cursor: pointer;
    } 
     @media (max-width: 1047px) {
        width: 100%;
        margin-right: 0;  
    }
`
const ProjectTitleDiv = styled.div`
    width: 170px;
    font-size: 1em;
    font-weight: bold;
`
const ProjectAssigneeDiv = styled.div`
    font-size: .9em;
    margin-top:0px;
`

const ProjectStatusBarDiv = styled.div`
    width: 80%;
`
const StatusDiv = styled.div`
    // background-color: blue;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-size: 1em;
    font-weight: bold;
    color: #999;
`

const BottomDiv = styled.div`
    height: 60%;
    display: flex;
    justify-content: space-between;
`
const ProjectCard = props => {
    var jobPercentage = ProjectController.getProjectPercentage(props.project.items)
    return (
        <ProjectCardDiv onClick={() => props.navigate(props.project)}>
            <div style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between'
            }}>
                <div style={{height: '40%'}}>
                    <ProjectTitleDiv>
                        {props.project.title}
                    </ProjectTitleDiv>
                    <ProjectAssigneeDiv>
                        <SmallAvatar name={TemplateController.getUserById(props.project.assignee).name}/>
                    </ProjectAssigneeDiv>
                </div>
                <div style = {{
                    width: '50px',
                    fontSize: '.8em',
                    color: '#999',
                    fontWeight: 'bold'
                }}>
                    Details  >
                </div>
            </div>
            <BottomDiv>
                <StatusDiv>
                    <div style={{marginBottom: '10px', fontSize: '.9em'}}>{jobPercentage}% Complete</div>
                    <ProjectStatusBarDiv>
                        <StatusBar percentage={jobPercentage}/>
                    </ProjectStatusBarDiv>
                </StatusDiv>
                {/*<BlueStakeDiv>*/}
                {/*<div style={{marginBottom: '0px', fontSize: '.9em'}}>BlueStakes</div>*/}
                {/*<ProjectStatusBarDiv>*/}
                {/*{getDate(props.project.blueStakeExpiration)}*/}
                {/*</ProjectStatusBarDiv>*/}
                {/*</BlueStakeDiv>*/}
            </BottomDiv>
        </ProjectCardDiv>
    )
}

const SmallAvatar = props => {
    return (
        <div>{props.name}</div>
    )
}

