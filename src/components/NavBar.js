import React from "react";
import styled from 'styled-components'
import {Color} from "../config/Config";

const WrapperDiv = styled.div`
    height: 75px;
    width: 100%;
    background-color: white;
    display: flex;
    flex-direction: row;
    align-items: center;
    @media (max-width: 700px) {
        background-color: ${Color.primary};
      }
`
const ActionButton = styled.div`
    width: 44px;
    height: 44px;   
    border-radius:50%;
    -moz-border-radius:50%;
    -webkit-border-radius:50%;
    display: flex;
    align-items: center;
    justify-content: center;
    // border: solid 1px ${Color.tint};
    color: white;
    font-size: 1.6em;
    background-color: ${Color.accent}
    font-weight: 800;
    box-shadow: 2px 2px 2px ${Color.accentShadow};
    @media (max-width: 700px) {
        box-shadow: 2px 2px 2px #222;
        
        width: 50px;
        height: 50px;  
        font-size: 2em;
    }
`


const ActionButtonContainer = styled.div`
    margin-left: auto;
    margin-right: 50px;
    @media (max-width: 700px) {
    margin-right: 16px;
    } 
`

const Avatar = props => {
    return (
        <ActionButton>
            +
        </ActionButton>
    )
}
const MenuButtonContainer = styled.div`
    margin-left: 16px;
`
export const NavBar = props => {
    return (
        <WrapperDiv>
            <MenuButtonContainer>
                <MenuButton/>
            </MenuButtonContainer>
            <ActionButtonContainer>

                <Avatar/>
            </ActionButtonContainer>
            {props.children}
        </WrapperDiv>
    )
}

const MenuButtonWrapper = styled.div`
    display: none;
    fill: #fff;
    @media (max-width: 700px) {
        display: block;
    }    
`
const StyledSvg = styled.svg`
    width: 24px;
    height: 24px;
`
export const MenuButton = (props) => {
    return (
        <MenuButtonWrapper>
            <StyledSvg viewBox='0 0 24 24'>
                <path d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z" />
            </StyledSvg>
        </MenuButtonWrapper>
    )
}