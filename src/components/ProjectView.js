import React from 'react'
import {withRouter} from "react-router-dom";
import {ListContainer} from "./ListContainer";
import styled from "styled-components";
import {Color} from "../config/Config";
import {StatusBar} from "./StatusBar";
import {ProjectController, TemplateController} from "../data/Model";
import {SquareStatusBar} from "./SquareStatusBar";

const HelloContainer = styled.div`
    height: 100%;
    width: 100%;
    overflow: auto;
    box-sizing: border-box;
    @media (max-width: 700px) {
        overflow-y: scroll; 
        -webkit-overflow-scrolling: touch; 
    }
`
const ListWrapper = styled.div`
    width: 100%;
    padding: 18px 50px 18px 50px;
    overflow: auto;
    box-sizing: border-box;
    @media (max-width: 700px) {
        padding: 18px;
        overflow-y: scroll; 
        -webkit-overflow-scrolling: touch; 
    }
`

const ProjectStatusBarDiv = styled.div`
    width: 95%;
`
const StatusDiv = styled.div`
    // background-color: blue;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-size: 1em;
    font-weight: bold;
    color: #999;
    @media (max-width: 700px) {
        display:none;
    }
`
const BottomStatusDiv = styled.div`
    // background-color: blue;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-size: 1em;
    font-weight: bold;
    color: #999; 
    @media (min-width: 699px) {
        display:none; 
    }
`

const BlueStakeDiv = styled.div`
    // background-color: blue;
    font-size: 1.2em;
    width:100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    
        align-items:center; 
        text-align: center;
    font-weight: bold;
    color: #999;
    @media (min-width: 699px) {
        align-items:flex-start; 
        text-align: left;
    }
`
const BottomDiv = styled.div`
    height: 50px;
    display: flex;
    margin: auto;
    justify-content: space-between;
`

const SSBContainerDiv = styled.div`
z-index:10;
    background-color: white; 
    border-top: solid 1px white;
    position: fixed;
    margin-top: 57px;
    width: calc(100vw - 250px);
    height: auto;
    display: flex;
    box-shadow: 1px 1px 2px #ddd;
    align-items: center;
    justify-content: space-between;
    color: ${Color.accent};
    text-transform: uppercase;
    text-align: left;
    font-size: 1.2em;
    font-weight: bold;
    display:none;
    @media (max-width: 700px) {
        display: inherit;
        width: 100vw;
        background-color: ${Color.primary}
        border-top: solid 1px ${Color.primary};
    }
`

class ProjectView extends React.Component {
    constructor(props) {
        super(props)
        var id = props.match.params.id
            this.state = {
                project: JSON.parse(localStorage.getItem('projects')).filter(p => p.id == id)[0],
            }

        this.updateProject = this.updateProject.bind(this)
    }

    componentWillMount() {
        ProjectController.init(() => {})
    }
    updateProject() {
        localStorage.setItem('projects', JSON.stringify(ProjectController.projects))
        var id = this.props.match.params.id
        this.setState({
            project: JSON.parse(localStorage.getItem('projects')).filter(p => p.id == id)[0],
        })
    }
    render() {
        var jobPercentage = ProjectController.getProjectPercentage(this.state.project.items)
        return (
            <HelloContainer>
                <Label percentage={jobPercentage} title={this.state.project.title} project={this.state.project}/>
                <SSBContainerDiv>
                    <SquareStatusBar percentage={jobPercentage}/>
                </SSBContainerDiv>
                <ListWrapper style={{paddingTop: '70px', paddingBottom: '100px'}}>

                    <BottomDiv>
                        {/*<BlueStakeDiv>*/}
                                {/*{getDate(this.state.project.blueStakeExpiration)}*/}
                        {/*</BlueStakeDiv>*/}
                    </BottomDiv>
                    <ListContainer updateProject={this.updateProject} items={this.state.project.items} project={this.state.project}/>
                </ListWrapper>
            </HelloContainer>
        )
    }
}

export default withRouter(ProjectView);

const LabelContainerDiv = styled.div`
z-index:10;
    background-color: white; 
    position: fixed;
    padding: 18px 68px 18px 50px;
    width: calc(100vw - 250px);
    height: auto;
    display: flex;
    align-items: center;
    box-sizing: border-box;
    justify-content: space-between;
    color: ${Color.accent};
    text-align: left;
    font-size: 1.2em;
    font-weight: bold;
    @media (max-width: 700px) {
        padding: 18px;
        width: calc(100vw - 36px);
        background-color: ${Color.primary}
        border-top: solid 1px ${Color.primary};
    }
`

const NavStatusWrapper = styled.div`
    width: 40%;
     @media (max-width: 700px) {
        display:none; 
    }
`
const StyledSvg = styled.svg`
    padding-left: 24px;
    width: 26px;
    height: 26px;
    webkit-tap-highlight-color: rgba(0,0,0,0),
    &:hover{
        cursor: pointer;
   `
export const Label = (props) => {
    var svgColor = '#999'
    return (
        <LabelContainerDiv>
            <div style={{width: '30%', textTransform: 'uppercase'}}>
                {props.title}
                <div style={{
                textAlign: 'left',
                fontSize: '.8em',
                color: '#999',
                textTransform: 'none',
                display: 'flex',
                justifyContent: 'flex-start'
                }}>
                    <div style={{display: 'flex', justifyContent: 'flex-end', flexDirection: 'column'}}>
                        <div>Assigned to {TemplateController.getUserById(props.project.assignee).name}</div>
                    </div>
                </div>
            </div>
            <NavStatusWrapper>
                <StatusDiv>
                    <div style={{marginBottom: '10px', fontSize: '.8em'}}>{props.percentage}% Complete</div>
                    <ProjectStatusBarDiv>
                        <StatusBar percentage={props.percentage}/>
                    </ProjectStatusBarDiv>
                </StatusDiv>
            </NavStatusWrapper>
            {/*<div style={{*/}
                {/*width: '30%',*/}
                {/*textAlign: 'left',*/}
                {/*fontSize: '.8em',*/}
                {/*color: '#999',*/}
                {/*textTransform: 'none',*/}
                {/*display: 'flex',*/}
                {/*justifyContent: 'center'*/}
            {/*}}>*/}
                    {/*<div style={{display:'flex', alignItems: 'flex-start', paddingRight: '6px', marginTop: '0px'}}>*/}
                        {/*<svg version="1.1"*/}
                                 {/*viewBox="0 0 24 24" style={{width: '18px', height: '18px'}} >*/}
                            {/*<path fill={svgColor} d="M12,4A4,4 0 0,1 16,8A4,4 0 0,1 12,12A4,4 0 0,1 8,8A4,4 0 0,1 12,4M12,14C16.42,14 20,15.79 20,18V20H4V18C4,15.79 7.58,14 12,14Z" />*/}
                        {/*</svg>*/}
                    {/*</div>*/}
                    {/*<div style={{display: 'flex', justifyContent: 'flex-end', flexDirection: 'column'}}>*/}
                        {/*<div>Assigned to {TemplateController.getUserById(props.project.assignee).name}</div>*/}
                    {/*</div>*/}
            {/*</div>*/}
            <div style={{width: '30%', textAlign: 'right', cursor: 'pointer'}}>
                <StyledSvg
                     viewBox="0 0 24 24">
                    <path fill={svgColor} d="M5,3C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V12H19V19H5V5H12V3H5M17.78,4C17.61,4 17.43,4.07 17.3,4.2L16.08,5.41L18.58,7.91L19.8,6.7C20.06,6.44 20.06,6 19.8,5.75L18.25,4.2C18.12,4.07 17.95,4 17.78,4M15.37,6.12L8,13.5V16H10.5L17.87,8.62L15.37,6.12Z" />
                </StyledSvg>

            </div>
        </LabelContainerDiv>
    )
}

const green = '#00bf0a'
const ValidDate = styled.div`
    text-align: left;
    color: ${green};
    display: flex;
    flex-direction: row;
    justify-content: center;
`
const yellow = "#ccbc00";
const UpdateDate = styled.div`
    text-align: left;
    color: ${yellow};
    display: flex;
    flex-direction: row;
    justify-content: center;
`
const red = "#f9001a"
const InvalidDate = styled.div`
    text-align: left;
    color: ${red};
    display: flex;
    flex-direction: row;
    justify-content: center;
`
const UpdateButtonDiv = styled.div`
    height:50px;
    width:100px;
    background-color: ${Color.button};
    box-shadow: 2px 2px 2px ${Color.buttonShadow};
    border-radius: 5px;
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;
`
const getDaysDiff = (firstDate, secondDate) => {
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    return Math.round((firstDate - secondDate)/(oneDay));
}
const getDate = (date) => {
    var today = new Date();
    var newDate = new Date(date);
    var dateString = (newDate.getMonth() + 1) + '/' + (newDate.getDate() - 2) ;
    var daysBetween = getDaysDiff(newDate, today);
    if (daysBetween >= 0 && daysBetween < 6) {
        return (
            <ValidDate>
                BlueStakes: Valid
            </ValidDate>
        )
    } else if (daysBetween < 0) {
        return (
            <InvalidDate>
                BlueStakes: Expired
            </InvalidDate>
        )
    } else if(daysBetween > 14) {
        return (
            <UpdateDate>
                BlueStakes: Requested
            </UpdateDate>
        )
    }
    else {
        return (
            <ValidDate>
                BlueStakes:    Valid
            </ValidDate>
        )
    }
}