import React from "react";
import styled from 'styled-components';
import {Color} from '../config/Config'
import {Link, withRouter} from "react-router-dom";
import {Auth} from 'aws-amplify'

const PaddingLeft = '35px';

const WrapperDiv = styled.div`
    height: 100vh;
    width: 250px;
    background-color: ${Color.primary};
    color: white;
    @media (max-width: 700px) {
        transform: translateX(-300px);
        position: fixed;
    }
`

const TitleDiv = styled.div`
    width: 100%;
    height: 90px;
    background-color: ${Color.tint};
    color: ${Color.titleText};
    display: flex;
    align-items: center;
    padding-left: ${PaddingLeft};
    font-size: 1.2em;
    font-weight: bold;
    border-bottom: solid 1px ${Color.primary}
    &:hover{
        cursor: default;
    }
     @media (max-width: 700px) {
        display: none;
    }
`

const NavWrapperDiv = styled.div`
    height: calc(100% - 75px);
    width: 100%;
    text-align: left;
    position: relative;
`
const MenuItemWrapperDiv = styled.div`
    z-index: 10;
    position: absolute;
    top: ${props => props.top};
    width: 80%;
    padding: 14px;
    padding-left: ${PaddingLeft};
    transition: all 0.2s ease;
    color: ${props => props.top === props.selectedPosition ? Color.accent : Color.lightColoredText}
    &:hover {
        color: ${Color.accent};
        cursor: pointer;
    }
`

const SignOutWrapperDiv = styled.div`
    z-index: 10;
    position: absolute;
    bottom: 30px;
    width: 100%;
    padding: 14px;
    padding-left: ${PaddingLeft};
    transition: all 0.2s ease;
    &:hover {
        cursor: pointer;
    }
`
const color = '#182533'
const SelectedCoverDiv = styled.div`
    display: ${props => props.show ? 'block' : 'none'}
    z-index: 1;
    width: 100%;
    height: 45px;
    top: ${props => props.top};
    left: 0;
    position: absolute;
    background-color: ${Color.selectedMenuItem};
    transition: all .3s cubic-bezier(.79,.19,.17,.83);
`
const ROUTES = [
    '/',
    '/projects',
    '/team-members',
    '/blue-stakes'
]

var getPosition = (route) => {
    var position = - 1
    for (var i = 0; i < ROUTES.length; ++i){
        if (ROUTES[i] === route)
            position = (i * 45)
    }
    return position.toString() + 'px'
}

class SideBar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedPosition: getPosition(props.location.pathname)
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            this.updateSelected(getPosition(this.props.location.pathname))
        }
    }
    updateSelected = (newPosition) => {
        this.setState({selectedPosition: newPosition})
    }
    render() {
        return (
            <WrapperDiv>
                <TitleDiv onClick={() => this.props.history.push('/')}>
                    Project Manager
                </TitleDiv>
                <NavWrapperDiv>
                    <SelectedCoverDiv top={this.state.selectedPosition} show={this.state.selectedPosition !== '-1px'}/>
                    <Link to="/"><MenuItemWrapperDiv
                        top='0px'
                        selectedPosition={this.state.selectedPosition}
                        onClick={() => this.updateSelected('0px')}>
                        Dashboard
                    </MenuItemWrapperDiv></Link>
                    <Link to="/projects"><MenuItemWrapperDiv
                        top='45px'
                        selectedPosition={this.state.selectedPosition}
                        onClick={() => this.updateSelected('0px')}>
                        Team Projects
                    </MenuItemWrapperDiv></Link>
                    <Link to="/my-projects"><MenuItemWrapperDiv
                        top='90px'
                        selectedPosition={this.state.selectedPosition}
                        onClick={() => this.updateSelected('45px')}>
                        My Projects
                    </MenuItemWrapperDiv></Link>
                    <Link to="/team-members"><MenuItemWrapperDiv
                        top='135px'
                        selectedPosition={this.state.selectedPosition}
                        onClick={() => this.updateSelected('90px')}>
                        Team Members
                    </MenuItemWrapperDiv></Link>
                    {/*<MenuItemWrapperDiv*/}
                        {/*top='135px'*/}
                        {/*selectedPosition={this.state.selectedPosition}*/}
                        {/*onClick={() => this.updateSelected('135px')}>*/}
                        {/*Reports*/}
                    {/*</MenuItemWrapperDiv>*/}
                    {/*<MenuItemWrapperDiv*/}
                        {/*top='180px'*/}
                        {/*selectedPosition={this.state.selectedPosition}*/}
                        {/*onClick={() => this.updateSelected('180px')}>*/}
                        {/*Settings*/}
                    {/*</MenuItemWrapperDiv>*/}
                    <SignOutWrapperDiv
                        selectedPosition={this.state.selectedPosition}
                        onClick={() => {
                        Auth.signOut()
                            .then(() => {
                                this.props.history.push('/auth')
                            })
                            .catch(() => console.log('error signing out...'))
                    }}>Sign Out</SignOutWrapperDiv>
                </NavWrapperDiv>
            </WrapperDiv>
        )
    }
}

export default withRouter(SideBar)
