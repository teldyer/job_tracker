import styled from 'styled-components'
import React from 'react'

const color = '#e01600'  //14685696
const color1= '#ff6700' //16738048
const color2 = '#d6c500' //14075136
const color3 = '#7dd600' //8246784
const color4 = '#07d635' //513589
const color5 = '#ee5b24'
const height = 20;
const Container = styled.div`
    position: absolute;
    width: 100%;
    height: ${height}px;
    overflow: hidden;
    border-radius: 0px;
    // border: solid 1px #eee;
    background-color: white;
    box-shadow: 0px 2px 2px #eee;
`
const Filler = styled.div`
    width: ${props => props.percentage + 1 + '%'};
    margin-left: -3px;
    margin-top: -2px;
    height: ${height + 5}px;
    background-color: hsl(${props => (props.percentage / 100) * 100}, 100%, 40%); 
    transition: all .5s ease-out;
    
`//2600x^2+119500x\ +15000900
const Percentage = styled.div`
    font-size: .9em;
    color: #555;
    position: absolute;
    margin-left: auto;
    margin-right: auto;
    top: 0;
    text-align: center;
    width: 100%;
    left: 0;
    right: 0;
`
export const SquareStatusBar = (props) => {
    // var decimal = -2600 * Math.pow(props.percentage, 2) + 119500 * props.percentage + 15000900
    // var hexString = decimal.toString(16);
    return (
        <Container>
            <Filler percentage={props.percentage}/>
            <Percentage>{props.percentage}%</Percentage>
        </Container>
    )
}