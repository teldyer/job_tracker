import styled from 'styled-components'
import React from 'react'

const color = '#e01600'  //14685696
const color1= '#ff6700' //16738048
const color2 = '#d6c500' //14075136
const color3 = '#7dd600' //8246784
const color4 = '#07d635' //513589
const color5 = '#ee5b24'
const height = 6;
const Container = styled.div`
    width: 100%;
    height: ${props=>props.height}px;
    overflow: hidden;
    border-radius: ${props=>props.height/2}px;
    // border: solid 1px #eee;
    box-shadow: 1px 1px 1px #ddd;
`
const Filler = styled.div`
    width: ${props => props.percentage + 1 + '%'};
    margin-left: -1px;
    margin-top: -2px;
    height: ${props=>props.height + 5}px;
    background-color: hsl(${props => (props.percentage / 106) * 102}, 100%, 38%); 
    transition: all .5s ease-out;
    
`//2600x^2+119500x\ +15000900
export const StatusBar = (props) => {
    // var decimal = -2600 * Math.pow(props.percentage, 2) + 119500 * props.percentage + 15000900
    // var hexString = decimal.toString(16);
    return (
        <Container height={props.height !== undefined ? props.height : height}>
            <Filler height={props.height !== undefined ? props.height : height} percentage={props.percentage + 1}/>
        </Container>
    )
}