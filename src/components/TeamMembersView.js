import React from 'react'
import styled from 'styled-components'
import {Color} from "../config/Config";
import {TemplateController} from "../data/Model";
import {PointerDiv, ViewBodyWrapper, ViewWrapper} from "../config/Components";
import {ViewHeader} from "./ViewHeader";
import {FixedViewHeader} from "./FixedViewHeader";


const StyledNewSvg = styled.svg`
margin-left: 4px;
    width: 24px;
    height: 24px;
    webkit-tap-highlight-color: rgba(0,0,0,0),
   `

export default class TeamMembersView extends React.Component {
    render() {
        return (
            <ViewWrapper>
                <FixedViewHeader>
                    <div style={{width: '30%', textTransform: 'uppercase'}}>
                        Team Members
                    </div>
                    {/*<PointerDiv style={{width: 'auto' , cursor: 'pointer', color: Color.accent, fontSize: '.9em', textAlign: 'right', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',}}>*/}
                        {/*Add Team Member*/}
                        {/*<StyledNewSvg*/}
                            {/*viewBox="0 0 24 24">*/}
                            {/*<path fill={Color.accent}  d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" />*/}
                        {/*</StyledNewSvg>*/}
                    {/*</PointerDiv>*/}
                </FixedViewHeader>
                <ViewBodyWrapper>

                    <ProjectCardContainer/>
                </ViewBodyWrapper>
            </ViewWrapper>
        )

    }
}

const LabelContainerDiv = styled.div`
    width: 100%;
    padding-bottom: 18px;
    display: flex;
    align-items: center;
    color: ${Color.accent};
    text-transform: uppercase;
    font-size: 1.2em;
    font-weight: bold;
    
`

const Label = props => {
    return (
        <LabelContainerDiv>
            {props.title}
        </LabelContainerDiv>
    )
}

const ProjectCardContainerDiv = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    flex-wrap: wrap;
`

class ProjectCardContainer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            users: []
        }
    }
    componentWillMount() {
        this.setState({users: TemplateController.users})
    }

    render() {
        return (
            <ProjectCardContainerDiv>
                {this.state.users.map(p => <TeamMemberCard user={p}/>)}
            </ProjectCardContainerDiv>
        )
    }
}

const ProjectCardDiv = styled.div`
    padding: 16px;
    width: 100%;
    height: 46px; 
    border-radius: 4px;
    background-color: white;
    margin-right: 18px;
    margin-bottom: 8px;
    text-align: left;
    // box-shadow: 0px 10px 14px #ddd;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    @media (max-width: 870px) {
        width: 100%;
        margin-right: 0;  
        height: auto;
        flex-wrap: wrap;
    }
`
const ProjectTitleDiv = styled.div`
    font-weight: bold;
`
const TeamMemberCard = props => {
    return (
        <ProjectCardDiv>
            <div style={{}}>
                <ProjectTitleDiv>
                    {props.user.name}
                </ProjectTitleDiv>
                <div style={{color: '#999', fontWeight: 'bold', fontSize: '.9em'}}>
                    {props.user.account_type}
                </div>
            </div>
        </ProjectCardDiv>
    )
}


