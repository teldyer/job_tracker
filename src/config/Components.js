import styled from "styled-components";


export const PointerDiv = styled.div`
    &:hover {
        cursor: pointer;
    }
`

export const ViewWrapper = styled.div`
    height: 100%;
    width: 100%;
    padding: 0px;
    box-sizing: border-box;
    min-height: 686px;
    @media (max-width: 700px) {
        // padding-bottom: 200px; 
        overflow-y: scroll; 
        -webkit-overflow-scrolling: touch; 
    }
    @media (max-height: 686px) {
        height: 100%;
        overflow-y: scroll; 
        -webkit-overflow-scrolling: touch; 
    }
`

export const ViewBodyWrapper = styled.div`
    padding: 30px;
    padding-top: 120px;
    box-sizing: border-box;
    height: 100%;
    width: 100%;
`