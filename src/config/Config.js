import {ProjectController} from "../data/Model";

export const Color = {
    primary: '#293542',
    tint: '#334252',
    separator: '#e9e9e9',
    background: '#f6f6f6',
    titleText: '#fdfbff',
    accent: '#ff0088',
    accentShadow: 'rgb(151, 0, 81, .3)',
    button: '#00ac2d',
    buttonShadow: '#7fcca4',
    // accent: '#ff6200',
    lightColoredText: '#dff5ff',
    selectedMenuItem: '#2e3c4b',
}

export const formatPhoneNumber = (number) => {
    if (!number || number.length < 10)
        return ''
    var areaCode = number.slice(2,5)
    var firstDigits = number.slice(5,8)
    var lastDigits = number.slice(8,12)
    return `(${areaCode}) ${firstDigits}-${lastDigits}`
}

export const checkProjectComplete = (project) => {
    if (ProjectController.getProjectPercentage(project.items) === 100) {
        var projectTitle = project.title.length > 21?  project.title.slice(0,21) + '...' : project.title
        var name = project.assignee_name.length > 15 ? project.assignee_name.slice(0, 15) + '...' : project.assignee_name
        var message = `\u2002    ${projectTitle}\n🎉\u0009Project completed\n\u0009by ${name}`
        fetch('http://18.237.167.72:3000/sms', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                to: ProjectController.notificationPhoneNumber,
                message: message
            }),
        })
            .then(function(response){

            }).then(function(body){

        });
    }
}