export class ProjectController {
    static notificationPhoneNumber = '+18017920961'
    static updateProject(project) {
        fetch('http://18.237.167.72:3000/project', {
                method: 'PATCH',
                headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                },
                body: project,
            },
        ).then(response => {
            if (response.ok) {
            }
            else {
            }
        });
    }

    static completeItem(projId, itemTitle, completedStatus) {
        var project = this.projects.filter(p => p.id === projId)[0];
        for (var i = 0; i < project.items.length; ++i) {
            if (project.items[i].title === itemTitle) {
                project.items[i].complete = completedStatus;
                project.items[i].date_complete = completedStatus ? new Date().getTime() : undefined;
            }
        }
        this.updateProject(JSON.stringify(this.projects.filter(p => p.id === projId)[0]))
    }

    static projects = [
        {
            id: 1,
            title: 'Johnson Mapleton',
            assignee: 1,
            blueStakeExpiration: 1545334920000,
            manager_phone_number: '+18017920961',
            items: [
                {
                    title: 'Blue Stake', complete: false,
                    type: 'item',
                    date_complete: 1545334920000
                },
                {
                    title: 'Sweep Sidewalks', complete: false,
                    type: 'item',
                    date_complete: 1545334920000
                },
                {
                    title: 'Prep Driveway', complete: false,
                    type: 'item',
                    date_complete: 1545334920000
                },
                {
                    title: 'Gravel Window Wells', complete: false,
                    type: 'item',
                    date_complete: 1545334920000
                },
                { title: 'Order Sewer Parts', complete: false,
                    type: 'item',
                    date_complete: 1545334920000 },
                { title: 'Dig Sewer', complete: false,
                    type: 'item',
                    date_complete: 1545334920000 },
                { title: 'Gravel Sewer', complete: false,
                    type: 'item',
                    date_complete: 1545334920000 },
                { title: 'Backfill Sewer', complete: false,
                    type: 'item',
                    date_complete: 1545334920000 },
                { title: 'Compact Sewer Trench', complete: false,
                    type: 'item',
                    date_complete: 1545334920000 },
            ]
        },
        {
            id: 2,
            title: 'Simmons 123 N 234 W Provo',
            assignee: 1,
            blueStakeExpiration: 1545334920000,
            manager_phone_number: '+18017920961',
            items: [
                {
                    title: 'Blue Stake', complete: true,
                    type: 'item'
                },
                {
                    title: 'Sweep Sidewalks', complete: true,
                    type: 'item'
                },
                {
                    title: 'Prep Driveway', complete: false,
                    type: 'item'
                },
                {
                    title: 'Gravel Window Wells', complete: false,
                    type: 'item'
                },

                { title: 'Order Sewer Parts', complete: false,
                    type: 'item' },
                { title: 'Dig Sewer', complete: false,
                    type: 'item' },
                { title: 'Gravel Sewer', complete: false,
                    type: 'item' },
                { title: 'Backfill Sewer', complete: false,
                    type: 'item' },
                { title: 'Compact Sewer Trench', complete: false,
                    type: 'item' },
            ]
        },
        {
            id: 3,
            title: 'Nelson 432 N 234 E Orem',
            assignee: 3,
            blueStakeExpiration: 1543606920000,
            manager_phone_number: '+18017920961',
            items: [
                {
                    title: 'Blue Stake', complete: false,
                    type: 'item'
                },
                {
                    title: 'Sweep Sidewalks', complete: true,
                    type: 'item'
                },
                {
                    title: 'Prep Driveway', complete: true,
                    type: 'item'
                },
                {
                    title: 'Gravel Window Wells', complete: true,
                    type: 'item'
                },

                { title: 'Order Sewer Parts', complete: false,
                    type: 'item' },
                { title: 'Dig Sewer', complete: false,
                    type: 'item' },
                { title: 'Gravel Sewer', complete: false,
                    type: 'item' },
                { title: 'Backfill Sewer', complete: false,
                    type: 'item' },
                { title: 'Compact Sewer Trench', complete: false,
                    type: 'item' },
            ]
        },
        {
            id: 4,
            title: 'Carson Lehi',
            assignee: 1,
            blueStakeExpiration: 1543261320,
            manager_phone_number: '+18017920961',
            items: [
                {
                    title: 'Blue Stake', complete: true,
                    type: 'item'
                },
                {
                    title: 'Sweep Sidewalks', complete: true,
                    type: 'item'
                },
                {
                    title: 'Prep Driveway', complete: true,
                    type: 'item'
                },
                {
                    title: 'Gravel Window Wells', complete: true,
                    type: 'item'
                },

                { title: 'Order Sewer Parts', complete: false,
                    type: 'item' },
                { title: 'Dig Sewer', complete: false,
                    type: 'item' },
                { title: 'Gravel Sewer', complete: false,
                    type: 'item' },
                { title: 'Backfill Sewer', complete: false,
                    type: 'item' },
                { title: 'Compact Sewer Trench', complete: false,
                    type: 'item' },
            ]
        },
        {
            id: 5,
            title: 'Heller 1234 S 1234 W Highland Utah 84003',
            assignee: 2,
            blueStakeExpiration: 1543606920000,
            manager_phone_number: '+18017920961',
            items: [
                {
                    title: 'Blue Stake', complete: true,
                    type: 'item'
                },
                {
                    title: 'Sweep Sidewalks', complete: true,
                    type: 'item'
                },
                {
                    title: 'Prep Driveway', complete: true,
                    type: 'item'
                },
                {
                    title: 'Gravel Window Wells', complete: true,
                    type: 'item'
                },

                { title: 'Order Sewer Parts', complete: false,
                    type: 'item' },
                { title: 'Dig Sewer', complete: false,
                    type: 'item' },
                { title: 'Gravel Sewer', complete: false,
                    type: 'item' },
                { title: 'Backfill Sewer', complete: false,
                    type: 'item' },
                { title: 'Compact Sewer Trench', complete: false,
                    type: 'item' },
            ]
        },
        {
            id: 6,
            title: 'Shelby Garage',
            assignee: 2,
            blueStakeExpiration: 1543261320,
            manager_phone_number: '+18017920961',
            items: [
                {
                    title: 'Order Parts', complete: true,
                    type: 'item'
                },
                {
                    title: 'Inspections', complete: true,
                    type: 'item'
                },
                {
                    title: 'Order Materials', complete: true,
                    type: 'item'
                },
            ]
        },
    ]

    static poll() {
        setTimeout(() => {
            this.init(() => {}) // would hit the API here
        }, 2000)
    }
    static init(callback) {
        console.log('init')
        fetch('http://18.237.167.72:3000/project', {
            method: 'GET',
                headers: {
                    Accept: 'application/json',
                },
            },
        ).then(response => {
            if (response.ok) {
                response.json().then(json => {
                    this.projects = json

                });

                localStorage.setItem('projects', JSON.stringify(this.projects))
                this.poll()
                callback()
            }
        });
    }


    static getProjectById(id) {
        var project = this.projects.filter(p => p.id === id)[0];
        project['assignee_name'] = TemplateController.getUserById(project.assignee).name
        return project
    }

    static getProjectPercentage(_items) {
        var completed = 0;
        var items = 0;
        var counts = this.getCounts(_items);
        completed += counts._completed
        items += counts._items
        return Math.round((completed / items) * 100);
    }

    static getCounts(itemList) {
        var completed = 0;
        var items = 0;
        for (var i = 0; i < itemList.length; ++i) {
            if (itemList[i].type === 'item') {
                items++
                if (itemList[i].date_complete) {
                    completed++
                }
            } else {
                var counts = this.getCounts(itemList[i].items);
                completed += counts._completed
                items += counts._items
            }
        }
        return {_items: items, _completed: completed}
    }
}



export class TemplateController {
    static getJobsForUser(username) {
        var user = this.users.filter(u => u.username === username)[0]
        return this.jobs.filter(j => user.jobs.includes(j.id))
    }

    static getNameForUser(username) {
        return this.users.filter(u => u.username === username)[0].name
    }
    static getUserById(id) {
        return this.users.filter(u => u.id === id)[0]
    }

    static getJobPercentage(job) {
        var completed = 0;
        var items = 0;
        var count;
        var templates = job.templates;
        // alert(JSON.stringify(templates))
        for (var i = 0; i < templates.length; ++i) {
            var templateId = templates[i]
            var template = this.templates.filter(t => t.id === templateId)[0]
            for (var j = 0; j < this.templates.length; ++j) {
                if (this.templates[j].id === templateId){
                    template = this.templates[j]
                }
            }
            if (template !== undefined) {
                count = this.getCounts(template);

                completed += count._completed
                items += count._items
            }
        }
        if (items === 0)
            items = 1;
        return Math.round((completed / items) * 100)
    }
    static getCounts(template) {
        var completed = 0;
        var items = 0;
        var itemList = template.items
        for (var i = 0; i < itemList.length; ++i) {
            if (itemList[i].type === 'item') {
                items++
                if (itemList[i].complete) {
                    completed++
                }
            } else {
                var counts = this.getCounts(itemList[i]);
                completed += counts._completed
                items += counts._items
            }
        }
        return {_items: items, _completed: completed}

    }

    static getItemsForJob(job) {
        return this.templates.filter(t => t.id === job.templates[0])[0].items
    }
    static templates = [
        {
            id: '1',
            title: 'backfill',
            type: 'template',
            items: [
                {
                    title: 'Blue Stake', complete: false,
                    type: 'item'
                },
                {
                    title: 'Sweep Sidewalks', complete: true,
                    type: 'item'
                },
                {
                    title: 'Prep Driveway', complete: true,
                    type: 'item'
                },
                {
                    title: 'Gravel Window Wells', complete: true,
                    type: 'item'
                },
                {
                    title: 'Sewer',
                    type: 'sublist',
                    items: [
                        { title: 'Order Parts', complete: true,
                            type: 'item' },
                        { title: 'Dig', complete: true,
                            type: 'item' },
                        { title: 'Gravel', complete: true,
                            type: 'item' },
                        { title: 'Backfill', complete: true,
                            type: 'item' },
                        { title: 'Compact', complete: true,
                            type: 'item' },
                    ]
                }
            ]
        },
        {
            id: '2',
            title: 'backfill',
            type: 'template',
            items: [
                {
                    title: 'Blue Stake', complete: false,
                    type: 'item'
                },
                {
                    title: 'Sweep Sidewalks', complete: false,
                    type: 'item'
                },
                {
                    title: 'Prep Driveway', complete: false,
                    type: 'item'
                },
                {
                    title: 'Gravel Window Wells', complete: true,
                    type: 'item'
                },
                {
                    title: 'Sewer',
                    type: 'sublist',
                    items: [
                        { title: 'Order Parts', complete: true,
                            type: 'item' },
                        { title: 'Dig', complete: false,
                            type: 'item' },
                        { title: 'Gravel', complete: false,
                            type: 'item' },
                        { title: 'Backfill', complete: true,
                            type: 'item' },
                        { title: 'Compact', complete: true,
                            type: 'item' },
                    ]
                }
            ]
        }
    ]

    static jobs = [
        {
            id: '1',
            title: 'H Bartholomew 3006 N 650 W PG',
            templates: [
                '1',
            ]
        },
        {
            id: '2',
            title: 'J2 732 N Country Manor Lane Alpine',
            templates: [
                '2',
            ]
        }

    ]

    static users = [
        {
            id: 1,
            name: 'John Lund',
            account_type: 'Manager',
            jobs: [
                '1',
                '2'
            ],
            username: 'johnwayne'
        },
        {
            id: 2,
            name: 'Devin Dyer',
            account_type: 'Manager',
            jobs: [
                '1',
                '2'
            ],
            username: 'devindyer'
        },
        {
            id: 3,
            name: 'Jason Willard',
            account_type: 'Team Member',
            jobs: [
                '1',
                '2'
            ],
            username: 'jasonwillard'
        },
    ]
}